# Yes/no questions:

## Should we check if packages *need* to be upgraded before upgrading?

### Pros:
- Removes unnecessary compilation
    - Better performance
    - Less bandwidth usage

### Cons:
- Increases complexity
- Introduces new questions (How do we check?, How often should we check?, Should this be a config option?)
- Probably not necessary for simple use cases

# Open discussion:

## How do we make dependency resolution faster?

Dependency resolution is absolutely the bottleneck of spkg. It can take as long as 10 seconds on some packages and sometimes even more than that.

The bottleneck within the resolving function appears to simply be the recursive nature of the problem. Basically, all the for loops add up quickly.

### What has been tried?
- Parallel does not improve performance that much and adds complexity
    - Could maybe be done by someone more experienced? idk
- Cannot be bothered to rewrite functionality in, say, Go for example.
    - Unclear if this would improve performance much
    - We'd also have to work around Slackware's lagging languages and their versions, even on current
    - Maybe a solution that is tail recursive could get us out of this pickle we're in

### Proposed solutions
- Give the user some output:
    - Instead of doing anything, give the user something to look at while they wait that way they know something is happening
    - Pro: Easiest solution apart from actually doing nothing
    - Con: Basically ignores the problem
- Retry previous solutions with new innovations:
    - Try parallelizing or rewriting again and see if there's anything that can be done
    - Pro: Could work out, you never know
    - Con: Vague. May not actually work
