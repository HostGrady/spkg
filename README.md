# spkg - slackbuilds package manager

spkg is a utility for easy installation of Slackbuilds. Though it is intended to work with *my* packages, it should work with any git based repository

To install the package simply move the package into the /sbin directory

# FAQ Time

Nobody has asked me these, but I expect they will become issues for people at some point so I may as well address them now

## Why?

1. I was unhappy with the current solutions
2. I wanted to try to make a package manager anyways
3. I made a custom fork of ponce's slackbuilds repo and I wanted to ensure that my package manager would work

## How do I use this?

Run ```./spkg``` as root for a detailed help menu. the man page (coming soon (maybe)) has the full details

## x is slow/could be done better

Probably true, *I* wrote it so that makes sense. 

And before you ask, yes I have tried to parallelize it.

If you have a better fix, send a pull request and ideally an issue to go along with it.

## Why do you build all packages instead of doing upgrades

It was simpler to do that rather than do a version check

### But doesn't that lead to unnecessary rebuilds?

Yes, I don't care, github can handle you downloading a bit more MB/GB than you should.

In addition, this project suits my needs, I don't install many complex packages from source so I don't deal with those consequences.

I'm not against rethinking this way of thinking in the future but at the moment it would involve complicating the project which is not a goal of mine at this stage.

## There's \<insert problem here\> how do I report a bug?

Go to the issues and submit a bug report.

Please ensure it is detailed and there are steps to reproduce the issue along with relevant system information.

## I want \<insert feature here\> how do I get it into spkg?

See above. Label it explicitly as a feature request.

Note that there's a chance I may ignore your feature request simply because it provides no immediate value to me

# Other things of note:

- License info is in the LICENSE file, it applies to any pieces of code or documentation in this repository (including this file)
- All contributors must *explicitly* agree to have their code licensed under identical terms or it will be rejected.`
- Obviously I am not responsible if this tool like fails horribly on you so please, take care.
- Non-default settings should be treated as risky/unsupported behaviour. There shouldn't be any issues but expect support to be a bit more iffy, especially if you use a seperate repository/
- spkg is not related to gitlab.com/Hostgrady/slackbuilds and any issues regarding that repository should be handled over there
- spkg is still pretty early and is not tested on every piece of equipment known to mankind. When submitting issues please be mindful of that and ideally, try to write a solution yourself if you know any bash.
