PREFIX = /usr/local

all: spkg

install: all
	cp -f spkg $(DESTDIR)$(PREFIX)/sbin
	chmod 755 $(DESTDIR)$(PREFIX)/sbin/spkg

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/sbin/spkg

.PHONY: all install uninstall
